<?php

namespace SchLabs\LaravelApiException\Traits;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Request;
use SchLabs\LaravelApiException\Exceptions\ApiInvalidCredentialsException;
use SchLabs\LaravelApiException\Exceptions\ApiMethodNotAllowedHttpException;
use SchLabs\LaravelApiException\Exceptions\ApiModelNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use ReflectionClass;
use SchLabs\LaravelApiException\Exceptions\ApiQueryException;
use SchLabs\LaravelApiException\Exceptions\ApiUnauthorizedHttpException;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Throwable;

trait ApiExceptionHandler
{

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param Throwable $e
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws Throwable
     */
    public function render($request, Throwable $e)
    {
        switch(get_class($e)) {
            case ModelNotFoundException::class:
                $exceptionName = $this->generateExceptionName($e->getModel());

                if(class_exists($exceptionName)){
                    $e = new $exceptionName;
                }
                break;
            case NotFoundHttpException::class:
                throw new ApiModelNotFoundException('Route not found: ' . Request::url());
                break;
            case QueryException::class:
                throw new ApiQueryException($e);
                break;
            case UnauthorizedHttpException::class:
                throw new ApiUnauthorizedHttpException('Token has expired');
                break;
            case MethodNotAllowedHttpException::class:
                throw new ApiMethodNotAllowedHttpException('The method is not supported for this route');
                break;
            case UnauthorizedException::class:
                throw new ApiUnauthorizedHttpException('User does not have the right roles');
                break;
            case InvalidCredentialsException ::class:
                throw new ApiInvalidCredentialsException('invalid credentials');
                break;
        }

        return parent::render($request, $e);
    }

    /**
     * Generates exception class name
     * @param $modelName
     * @return string
     * @throws \ReflectionException
     */
    private function generateExceptionName($modelName): string
    {
        $reflect = new ReflectionClass($modelName);
        $shortName = $reflect->getShortName();
        $path = "App\\Exceptions\\";
        return "$path{$shortName}NotFoundException";
    }

}
