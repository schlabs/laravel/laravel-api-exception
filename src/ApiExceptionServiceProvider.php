<?php

namespace SchLabs\LaravelApiException;

use Illuminate\Support\ServiceProvider;

class ApiExceptionServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }

    public function register()
    {

    }
}
