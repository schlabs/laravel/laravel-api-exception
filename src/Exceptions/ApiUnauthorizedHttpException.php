<?php

namespace SchLabs\LaravelApiException\Exceptions;

class ApiUnauthorizedHttpException extends ApiException
{
    public function __construct($message = "Token has expired")
    {
        parent::__construct($message, 412, 412);
    }

}
