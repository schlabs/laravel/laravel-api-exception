<?php

namespace SchLabs\LaravelApiException\Exceptions;

class ApiModelNotFoundException extends ApiException
{
    public function __construct($message = "Model not found")
    {
        parent::__construct($message, 404, 404);
    }
}
