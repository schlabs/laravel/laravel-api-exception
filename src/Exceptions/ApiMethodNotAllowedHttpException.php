<?php

namespace SchLabs\LaravelApiException\Exceptions;

class ApiMethodNotAllowedHttpException  extends ApiException
{
    public function __construct($message = "The method is not supported for this route")
    {
        parent::__construct($message, 405, 405);
    }

}
