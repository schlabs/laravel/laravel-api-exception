<?php

namespace SchLabs\LaravelApiException\Exceptions;

class ApiInvalidCredentialsException extends ApiException
{
    public function __construct($message = "invalid credentials")
    {
        parent::__construct($message, 401, 401);
    }

}
