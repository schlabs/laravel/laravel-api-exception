<?php
namespace SchLabs\LaravelApiException\Exceptions;

use Exception;
use SchLabs\LaravelApiResponse\Traits\ApiResponse;

class ApiException extends Exception
{
    use ApiResponse;

    private $status;

    public function __construct($message = "", $code = 0, $status = 400, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->setStatus($status);
    }

    public function render()
    {
        return $this->failure(
            $this->getMessage(),
            $this->getCode(),
            $this->getStatus()
        );
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

}
