<?php

namespace SchLabs\LaravelApiException\Exceptions;

class ApiQueryException extends ApiException
{
    public function __construct(\Throwable $ex)
    {
        if(in_array(config('app.env'), ['dev', 'local'])){
            parent::__construct($ex , 500, 500);
        }else{
            parent::__construct("Server error", 500, 500);
            \Log::error('Server error (ApiQueryException)', $ex->getMessage());
        }

    }
}
